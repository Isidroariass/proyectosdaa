# Proyecto SDAA: Estación meteorológica móvil

Para iniciar todos los procesos necesarios, incluidos la página web, el broker mqtt, el middleware ... solo es necesario ejecutar el  comando *make all* 

El esquema de comunicaciones entre los procesos es el siguiente:

![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")
